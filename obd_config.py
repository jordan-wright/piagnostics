 #!/usr/bin/env python
###########################################################################
# obd_config.py
#
#
#   Updated, heavily modified by piAgnostics team (Texas Tech University)
#       Cameron Gunter
#       Chris Raley
#       Austin Whittington
#       Jordan Wright
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of pyOBD.
#
# pyOBD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyOBD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyOBD; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################

"""To select which elements you want to display,
  simply uncomment the fields you want to use"""
custom_config = [
  #'pids',
  #'dtc_status',
  #'dtc_ff',
  #'fuel_status',
  #'load',
  #'temp',
  #'short_term_fuel_trim_1',
  #'long_term_fuel_trim_1',
  #'short_term_fuel_trim_2',
  #'long_term_fuel_trim_2',
  #'fuel_pressure', 
  #'manifold_pressure',
  #'rpm',
  'speed',
  #'timing_advance',
  #'intake_air_pump',
  #'maf',
  #'throttle_pos',
  #'secondary_air_status',
  #'o2_sensor_positions',
  #'o211',
  #'o212',
  #'o213',
  #'o214',
  #'o221',
  #'o222',
  #'o223',
  #'o224',
  #'obd_standard',
  #'o2_sensor_position_b',
  #'aux_input',
  #'engine_time',
  #'engine_mil_time'
  ]
  
mechanic_config = [
  'pids',
  'dtc_status',
  'dtc_ff',
  'fuel_status',
  'load',
  'temp',
  'short_term_fuel_trim_1',
  'long_term_fuel_trim_1',
  'short_term_fuel_trim_2',
  'long_term_fuel_trim_2',
  'fuel_pressure', 
  'manifold_pressure',
  'rpm',
  'speed',
  'timing_advance',
  'intake_air_temp',
  'maf',
  'throttle_pos',
  'secondary_air_status',
  'o2_sensor_positions',
  'o211',
  'o212',
  'o213',
  'o214',
  'o221',
  'o222',
  'o223',
  'o224',
  'obd_standard',
  'o2_sensor_position_b',
  'aux_input',
  'engine_time',
  'engine_mil_time'
  ]
  
user_config = [
  'temp',
  'rpm',
  'speed',
  'engine_time'
  ]
 #!/usr/bin/env python
###########################################################################
# obd_io.py
#
#   Updated, heavily modified by piAgnostics team (Texas Tech University)
#       Cameron Gunter
#       Chris Raley
#       Austin Whittington
#       Jordan Wright
#
#
# Original (base) library components provided by:
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of pyOBD.
#
# pyOBD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyOBD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyOBD; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################

import serial
import obd_sensors
import time

class OBDPort:
    """ OBDPort abstracts all communication with OBD-II device."""
    def __init__(self,portnum,SERTIMEOUT,RECONNATTEMPTS):
        """Initializes port by resetting device and gettings supported PIDs. """
        # These should really be set by the user.
        # NOTE: This works, but let's find the protocol spec for these values.
        self.baud     = 38400
        self.databits = 8
        self.par      = serial.PARITY_NONE  # parity
        self.sb       = 1                   # stop bits
        self.to       = SERTIMEOUT
        self.ELMver = "Unknown"
        self.connected = False #state SERIAL is 1 connected, 0 disconnected (connection failed)
        self.port = None
        self.portnum = portnum
        self.reconnect_attempts = RECONNATTEMPTS
    
    def connect(self):
        """ Connect to the Serial Port """
        try:
            print  "Opening interface (serial port)"
            self.port = serial.Serial(self.portnum,self.baud, \
            parity = self.par, stopbits = self.sb, bytesize = self.databits,timeout = self.to)
        except serial.SerialException as e:
            print e
            self.connected = False # Will this ever be needed?
            return False
        print  "Interface successfully " + self.port.portstr + " opened"
        print  "Connecting to ECU..."
        # Connect to the ELM Software
        try:
            for i in range (self.reconnect_attempts):
                result = self.send_command("atz")
                self.send_command("ate0")
                if self.check_connect(result):   # initialize
                    print '[+] Connected to ' + self.portnum
                    return self.connected
                print '[!] Unable to connect... Retrying'
                time.sleep(1)
            return False
        except serial.SerialException:
            pass
        return False
    
    def send_command(self, cmd):
        """ Send command to serial port """
        if self.port:
            self.port.flushOutput()
            self.port.flushInput()
            for c in cmd:
                self.port.write(c)
            self.port.write("\r\n")
            print "Send command:" + cmd
        return self.get_result()
    
    def get_result(self):
        """ Get the result from sending the command """
        repeat_count = 0
        if self.port is not None:
            buffer = ""
            while 1:
                c = self.port.read(1)
                if len(c) == 0:
                    'No Response.. Polling..'
                    if(repeat_count == 3):
                        break
                        return False
                    repeat_count = repeat_count + 1
                    continue
                if c == '\r':
                    continue
                if c == ">":
                    break;
                if buffer != "" or c != ">": #if something is in buffer, add everything
                    buffer = buffer + c
            #print "Got result: " + buffer
            if(buffer == ""):
                return None
            result = buffer.strip().split()
            if result[0] == '41':
                return ''.join(result[2:])
            else:
                return ''.join(result)
        else:
            print  "No OBD Port configured (have you run obd.connect()?)"
        return None
    
    def check_connect(self, result=None):
        """ Check to see if our connect function successfully connected """
        if not result: result = None
        if not self.connected:
            if result:
                self.connected = True
        return self.connected

    def close(self):
        """ Resets device and closes all associated filehandles"""
        if (self.port!= None) and self.connected==True:
            self.send_command("atz")
            self.port.close()        
        self.port = None
        self.ELMver = "Unknown"
#!/usr/bin/env python

import obd_config
import argparse
from obd_io import OBDPort
from obd_sensors import queries

def main():
	#set command line arguments for config files
	parser = argparse.ArgumentParser(description="RaspberryPi Compatible Python OBD II Library")
	group = parser.add_mutually_exclusive_group()
	group.add_argument('-u','--user_config', action='store_const', dest='config', const=obd_config.user_config)
	group.add_argument('-m','--mech_config', action='store_const', dest='config', const=obd_config.mechanic_config)
	group.add_argument('-c','--custom_config', action='store_const', dest='config', const=obd_config.custom_config)
	args = parser.parse_args()
	
	#default to user config
	if not args.config: 
		args.config = obd_config.user_config

	# The appropriate configuration is in args.config
	obd_port = OBDPort('/dev/pts/6',2,1)
	
	if not obd_port.connect():
		'[!] Critical Error - Unable to connect... Exiting'
		exit(1)
	 
	#start main loop 
	main_loop(obd_port, args)
	  
def main_loop(obd_port, args):
	#load report string with headers
	r_string = (','.join(str(cmd) for cmd in args.config)) + '\n'
	while(1):
		try:
			#go through each query, sending -> recieving -> printing each value, storing data in the report string 
			for index, cmd in enumerate(args.config):
				value=str(queries[cmd].handle(obd_port.send_command(queries[cmd].cmd)))
				print queries[cmd].desc + ': ' + value	+ ' ' + queries[cmd].unit
				r_string += str(value)
				if(index != len(args.config)-1):
					r_string += ','
			r_string += '\n'
		except KeyboardInterrupt:
			#catch keyboard interrupt and go to menu
			con = args.config
			menu(r_string, args)
			#clear report string if config changes
			if con != args.config:
				r_string = ''
				r_string = (','.join(str(cmd) for cmd in args.config)) + '\n'

def menu(r_string, args): 
	#display choices
	print 'Config Change: \n'
	print 'a. User Config\n'
	print 'b. Mechanic Config\n'
	print 'c. Custom Config\n'
	print 'd. Abort Config Change\n'
	print 'e. Generate Report\n'
	print 'f. Exit Program\n'
	c = raw_input('Choice: ')
	
	#choice dictionary
	choices = {
		'a' : obd_config.user_config,
		'b' : obd_config.mechanic_config,
		'c' : obd_config.custom_config,
		'd' : 'no',
		'e' : report_gen,
		'f' : exit
	}
	
	#wait for a valid choice
	while c not in choices:
		c = raw_input('Choice: ')

	#call the command or change the config
	if(c=='f'):
		choices[c]()
	elif(c=='e'):
		choices[c](r_string)
	elif(c!='d'):
		args.config=choices[c]
		
def report_gen(r_string):
	#write the r_string to the report file
	with open('report.csv', 'w') as report:
		report.write(r_string)

if __name__ == "__main__":
	main()
